//
//  AppDelegate.h
//  example1
//
//  Created by jefferykao on 2014/1/14.
//  Copyright (c) 2014年 jefferykao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
